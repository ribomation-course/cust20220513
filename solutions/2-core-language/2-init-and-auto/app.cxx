#include <iostream>
#include <string>
#include <vector>
#include <set>

using namespace std::string_literals;
using std::cout;
using std::string;

int main() {
    auto v = std::vector<string>{
        "anna"s, "bertil"s, "carin"s, "david"s, "eva"s
    };
    for (auto const& name : v) cout << name << " ";

    cout << "\n--\n";
    auto s = std::set<string>{
            "eva"s,
            "bertil"s, 
            "anna"s, 
            "david"s, 
            "carin"s 
    };
    for (auto const& name : s) cout << name << " ";
}
