#include <iostream>
#include <string>

using std::cout;
using std::string;

struct Person {
    string   name = "no name";
    unsigned age  = 18;

    Person() {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    Person(string const& n, unsigned a) : name{n}, age{a} {
        cout << "Person{" << name << ", " << age << "} @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person{} @ " << this << "\n";
    }
};


void usecase1() {
    cout << "-- (1) --\n";
    auto ptr = new int{42};
    cout << *ptr << " @ " << ptr << "\n";
    delete ptr;
}

void usecase2() {
    cout << "-- (2) --\n";
    auto const N   = 25U;
    auto       arr = new int[N];
    for (auto  k   = 0U; k < N; ++k) arr[k] = k + 1;
    for (auto  k                            = 0U; k < N; ++k) cout << arr[k] << " ";
    cout << "\n";
    delete[] arr;
}

void usecase3() {
    cout << "-- (3) --\n";
    auto p1 = new Person{};
    cout << "p1: " << p1->name << ", " << p1->age << "\n";

    auto p2 = new Person{"Nisse", 42};
    cout << "p2: " << p2->name << ", " << p2->age << "\n";

    delete p1;
    delete p2;
}

void usecase4() {
    cout << "-- (4) --\n";
    auto const N   = 5U;
    auto       arr = new Person[N];
    for (auto  k   = 0U; k < N; ++k) {
        auto p = &arr[k];
        cout << "p: " << p->name << ", " << p->age << "\n";
    }
    delete[] arr;
}

int main() {
    usecase1();
    usecase2();
    usecase3();
    usecase4();
}
