#pragma once
#include <string>
#include <optional>
#include <utility>

namespace ribomation::utils {
    using std::string;
    using std::optional;
    using namespace std::string_literals;
    
    struct Address {
        optional<string> street_;
        optional<string> post_code_;
        optional<string> city_;
        Address() = default;
        auto street(const string& s)    -> Address& {street_ = s; return *this;}
        auto post_code(const string& s) -> Address& {post_code_ = s; return *this;}
        auto city(const string& s)      -> Address& {city_ = s; return *this;}
        
        [[nodiscard]] auto to_string() const -> string {
            return street_.value_or("") + ", "s + post_code_.value_or("") + " " + city_.value_or("");
        }
    };

    struct Contact {
        const string name;
        optional<string> email_;
        optional<string> twitter_;
        optional<Address> address_;

        explicit Contact(string n) : name{std::move(n)} {}
        auto email(const string& s) {email_ = s; return *this;}
        auto twitter(const string& s) {twitter_ = s; return *this;}
        auto address(const Address& s) {address_ = s; return *this;}

        [[nodiscard]] auto to_string() const -> string {
            return "Contact{"s
                   + name
                   + ", email="s + email_.value_or("")
                   + ", twitter="s + twitter_.value_or("")
                   + ", address="s + address_->to_string()
                   + "}"s;
        }
    };
    
}

