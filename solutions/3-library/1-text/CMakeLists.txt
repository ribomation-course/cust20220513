cmake_minimum_required(VERSION 3.18)
project(1_text)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(app app.cxx)
target_compile_options(app PRIVATE ${WARN})


