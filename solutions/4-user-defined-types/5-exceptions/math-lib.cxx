#include <sstream>
#include <cmath>
#include "math-lib.hxx"

namespace ribomation::math {
    using namespace std::string_literals;

    auto log_2(double x) -> double {
        auto result = std::log2(x);
        if (std::isfinite(result)) return result;
        if (std::isinf(result)) throw InfError{};
        if (std::isnan(result)) throw NaNError{};
        throw MathError{"log2: unknown error"s};
    }
}
