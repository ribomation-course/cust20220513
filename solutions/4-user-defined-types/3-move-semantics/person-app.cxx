#include <iostream>
#include <string>
#include <vector>
#include "person.hxx"

namespace rm = ribomation::domain;
using std::cout;

auto func(rm::Person q) -> rm::Person {
    std::cout << "[func] q: " << q.toString() << "\n";
    q.incrAge();
    std::cout << "[func] q: " << q.toString() << "\n";
    return q;
}

void usecase1() {
    cout << "[1] -- single object ----\n";
    
    auto p = rm::Person{"Cris P. Bacon", 27};
    cout << "[1] p: " << p.toString() << "\n";
}

void usecase2() {
    cout << "[2] -- three objects ----\n";

    auto p = rm::Person{"Cris P. Bacon", 27};
    cout << "[2] p: " << p.toString() << "\n";

    auto p2 = rm::Person{std::move(p)};
    cout << "[2] p2: " << p2.toString() << "\n";
    cout << "[2] p: " << p.toString() << "\n";

    auto p3 = rm::Person{};
    cout << "[2] p3: " << p3.toString() << "\n";

    p3 = std::move(p2);
    cout << "[2] p3: " << p3.toString() << "\n";
    cout << "[2] p2: " << p2.toString() << "\n";
    cout << "[2] p: " << p.toString() << "\n";
}

void usecase3() {
    cout << "[3] -- function call ----\n";

    auto p = rm::Person{"Cris P. Bacon", 27};
    cout << "[3] p: " << p.toString() << "\n";
    
    auto q = func(std::move(p));
    cout << "[3] q: " << q.toString() << "\n";
    cout << "[3] p: " << p.toString() << "\n";
}

void usecase4() {
    cout << "[4] -- std::vector ----\n";

    auto v = std::vector<rm::Person>{};
    v.reserve(3);
    v.emplace_back("Anna",  27);
    v.emplace_back("Berit", 37);
    v.emplace_back("Carin", 47);

//    cout << "[4] -- print: element by-copy ----\n";
//    for (auto p : v) cout << "[cpy] p: " << p.toString() << "\n";

    cout << "[4] -- print: element by-const-ref ----\n";
    for (auto const& p : v) cout << "[&]  p: " << p.toString() << "\n";
    
    cout << "[4] -- print: element by-ref-ref ----\n";
    for (auto&& p : v) cout << "[&&]  p: " << p.toString() << "\n";
    
    cout << "[4] -- just before end of block ----\n";
}

auto g = rm::Person{"Anna Conda", 42};
int main() {
    cout << "[main] enter\n";
    cout << "[main] g: " << g.toString() << "\n";

    usecase1();
    usecase2();
    usecase3();
    usecase4();

    cout << "[main] exit\n";
}
