#include <iostream>
#include <algorithm>

using std::cout;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    std::for_each(&numbers[0], &numbers[N], [](auto n) {
        cout << n << " ";
    });
    cout << "\n";
    
    auto factor = 42;
    std::transform(numbers, numbers+N, numbers, [factor](auto n){
        return n * factor;
    });
    
    std::for_each(&numbers[0], &numbers[N], [](auto n) {
        cout << n << " ";
    });
}
