# Homework 1

## Preparation
1. Review the course material, so far.
1. Complete any excerice(s) you didn't had the time to during the lecture day

# Task Description

## (a) Print Fizz-Buzz
Implement a function that prints out Fizz-Buzz, according to

* https://en.wikipedia.org/wiki/Fizz_buzz

Ensure you are using Modern style C++, with auto etc.

The function signature, should be

    void fizzbuzz(unsigned maxNumber=100, unsigned first=3, unsigned second=5) 

which means, it should be possible to use another set of numbers, 
other than the classic 3, 5 and 15 (3*5). Try it as well.

## (b) Return Fizz-Buzz as a `std::vector<std::string>`
After you have verified it's working, change the function to return by-value

    std::vector<std::string>

and, print out the returned string vector

# Hints:

* Include `<string>`, `<vector>`
* Use `std::to_string()`, to convert a number to a string
* Use member function `push_back()` to insert an element at the end of the vector


# Solution

* [`fizz-buzz.cxx`](./solution/fizz-buzz.cxx)
* [CMake file](./solution/CMakeLists.txt)

## Usage

    $ cd path/to/your/hw1/solution/
    $ cmake -S . -B bld
    $ cmake --build bld
    $ ./bld/fizz-buzz

### Change the number of turns

    $ ./bld/fizz-buzz -n 100

### Change the fizz and buzz factors

    $ ./bld/fizz-buzz -f 3 -b 5

### Collect into a `std::vector<std::string>` and print that one

    $ ./bld/fizz-buzz -vec

