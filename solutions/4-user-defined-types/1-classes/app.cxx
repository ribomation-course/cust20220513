#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using namespace std::string_literals;

struct Dog {
    Dog(string n) : name{std::move(n)} { ++cnt; }
    Dog(Dog const& that) : name{that.name} {++cnt;}

    ~Dog() { --cnt; }

    [[nodiscard]] string to_string() const {
        return "Dog{"s + name + "}"s;
    }

    static int count() { return cnt; }

private:
    const string name;
    static int   cnt;
};

int Dog::cnt = 0;

class Person {
    string   name;
    unsigned age;
    Dog* dog = nullptr; //should be initialized, else it might have a random address value ;-/
public:
    Person(string n, unsigned a) : name{std::move(n)}, age{a} {}

    void set(Dog* d) { dog = d; }

    [[nodiscard]] string to_string() const {
        auto buf = std::ostringstream{};
        buf << "Person{" << name << ", " << age
            << (dog ? ", "s + dog->to_string() : ""s)
            << "}";
        return buf.str();
    }
};

int main() {
    {
        auto fido  = Dog{"Fido"s};
        auto nisse = Person{"Nisse Hult"s, 42};
        cout << nisse.to_string() << "\n";

        nisse.set(&fido);
        cout << nisse.to_string() << "\n";
    }

    cout << "---\n# dogs = " << Dog::count() << "\n";
    {
        auto dogs = std::vector<Dog>{
                {"Caro"s}, {"Fido"s}, {"Lassie"s}, {"Milou"s} 
        };
        for (auto const& d : dogs) cout << d.to_string() << " ";
        cout << "\n# dogs = " << Dog::count() << "\n";
    }
    cout << "# dogs = " << Dog::count() << "\n";
}
