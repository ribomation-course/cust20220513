#pragma once
#include <string>
#include "employee.hxx"

namespace ribomation::persons {
    using namespace std::string_literals;
    using std::string;

    struct Manager : Employee {
        Manager(string name_, unsigned id, string dept) 
            : Employee{std::move(name_), id}, department{std::move(dept)} {}

        [[nodiscard]] auto to_string() const -> string override {
            return "Manager{dept="s + department + " "s + Employee::to_string() + "}"s;
        }

    private:
        const string department;
    };

}


