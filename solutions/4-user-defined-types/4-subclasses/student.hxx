#pragma once
#include <string>
#include "person.hxx"

namespace ribomation::persons {
    using namespace std::string_literals;
    using std::string;

    struct Student : Person {
        Student(string name_, unsigned id) 
            : Person{std::move(name_)}, student_id{id} {}

        [[nodiscard]] auto to_string() const -> string override {
            return "Student{id="s + std::to_string(student_id) + " "s + Person::to_string() + "}"s;
        }

    private:
        unsigned student_id;
    };

}


