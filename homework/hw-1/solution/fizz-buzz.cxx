#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using namespace std::string_literals;

void fizzbuzz(unsigned N = 100, unsigned fizz = 3, unsigned buzz = 5);
auto fizzbuzz2(unsigned N = 100, unsigned fizz = 3, unsigned buzz = 5) -> std::vector<string>;

int main(int argc, char** argv) {
    auto max  = 100U;
    auto fizz = 3U;
    auto buzz = 5U;
    auto vec  = false;

    for (auto k = 1; k < argc; ++k) {
        auto arg = std::string{argv[k]};
        if (arg == "-n"s) {
            max = std::stoi(argv[++k]);
        } else if (arg == "-f"s) {
            fizz = std::stoi(argv[++k]);
        } else if (arg == "-b"s) {
            buzz = std::stoi(argv[++k]);
        } else if (arg == "-vec"s) {
            vec = true;
        } else {
            std::cerr << argv[0] << ": [-n <max>] [-f <fizz>] [-b <buzz>] [-vec]\n";
            return 1;
        }
    }

    if (vec) {
        auto result = fizzbuzz2(max, fizz, buzz);
        for (auto const& line: result) cout << '"' << line << "\"\n";
    } else {
        fizzbuzz(max, fizz, buzz);
    }
}

void fizzbuzz(unsigned N, unsigned fizz, unsigned buzz) {
    auto const fizzbuzz = fizz * buzz;
    for (auto  k    = 1U; k <= N; ++k) {
        if (k % fizzbuzz == 0) {
            cout << "fizzbuzz(" << k << ")\n";
        } else if (k % buzz == 0) {
            cout << "buzz(" << k << ")\n";
        } else if (k % fizz == 0) {
            cout << "fizz(" << k << ")\n";
        } else {
            cout << k << "\n";
        }
    }
}

auto fizzbuzz2(unsigned N, unsigned fizz, unsigned buzz) -> std::vector<string> {
    auto       result = std::vector<string>{};
    auto const fizzbuzz   = fizz * buzz;
    for (auto  k      = 1U; k <= N; ++k) {
        auto buf = std::ostringstream{};
        if (k % fizzbuzz == 0) {
            buf << "fizzbuzz(" << k << ")";
        } else if (k % buzz == 0) {
            buf << "buzz(" << k << ")";
        } else if (k % fizz == 0) {
            buf << "fizz(" << k << ")";
        } else {
            buf << k;
        }
        result.push_back(buf.str());
    }
    return result;
}
