#pragma once

#include <string>
#include <utility>

namespace ribomation::persons {
    using std::string;
    using namespace std::string_literals;

    struct Person {
        virtual ~Person() = default;
        
        [[nodiscard]] virtual auto to_string() const -> string {
            return "Person{"s + name + "}"s;
        }
        
        explicit Person(string name_) : name{std::move(name_)} {}
    protected:

    private:
        const string name;
    };

}


