#include <iostream>
#include <fstream>
#include <filesystem>
#include <stdexcept>
#include <unordered_set>
#include <string>

namespace fs = std::filesystem;
using namespace std::string_literals;
using std::cout;
using std::string;
unsigned linecount(fs::path const& path);

int main(int argc, char** argv) {
    auto dir = argc == 1 ? fs::current_path() : fs::path{argv[1]};
    if (!fs::is_directory(dir))
        throw std::invalid_argument{"not a dir: "s + dir.string()};

    auto EXTS     = std::unordered_set<string>{
            ".txt"s, ".cxx"s, ".hxx"s
    };
    auto totalCnt = 0U;
    for (auto const& entry: fs::recursive_directory_iterator{dir}) {
        const auto& filename = entry.path();
        if (fs::is_regular_file(filename)) {
            if (EXTS.count(filename.extension()) == 1) {
                auto cnt = linecount(filename);
                totalCnt += cnt;
                cout << filename << ": " << cnt << " lines\n";
            }
        }
    }
    cout << "TOTAL: " << totalCnt << " lines\n";
}

unsigned linecount(fs::path const& path) {
    auto cnt = 0U;
    auto f   = std::ifstream{path};
    if (!f) throw std::runtime_error{"cannot open for read: "s + path.string()};

    for (string line; std::getline(f, line);) ++cnt;

    return cnt;
}
