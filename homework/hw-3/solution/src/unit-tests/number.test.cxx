#include "catch.hpp"
#include "number.hxx"
#include <sstream>
using namespace ribomation;
using namespace std;
using namespace std::literals;

TEST_CASE("default constructor should return zero", "[number]") {
    auto target = Number<5>{};
    REQUIRE(target.value() == 0);
}

TEST_CASE("insert small int", "[number]") {
    auto target = Number<5>{42};
    REQUIRE(target.value() == 42);

    auto str = string{target.getStorage(), 5};
    REQUIRE(str == "00042"s);
}

TEST_CASE("insert large int", "[number]") {
    auto target = Number<3>{};
    target = 12345;
    REQUIRE(target.value() == 123);

    auto str = string{target.getStorage(), 3};
    REQUIRE(str == "123"s);
}

TEST_CASE("insert negative int", "[number]") {
    auto target = Number<5>{-17};
    REQUIRE(target.value() == -17);

    auto str = string{target.getStorage(), 5};
    REQUIRE(str == "-0017"s);
}
