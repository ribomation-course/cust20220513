# Homework 2
## Preparation
1. Review the course material, so far.
1. Complete any excerice(s) you didn't had the time to during the lecture day

# Task Description
Write a C++ program that:
* Opens a text file, were its name is provided on the command-line
* Extracts all words (keep just the letters)
* Aggregates the word frequencies (word size >= W)
* Sorts them in descending frequency order
* Keeps the N most frequent words
* Converts each word-frequency pair into a HTML <span> tag, with an inline style setting the size proportional to the frequency
* Writes out a complete HTML file with the words, to a file named as the input file but with the extension replaced by .html
* Finally, it should print out
    - Input file size
    - Number of words collected
    - Elapsed time in seconds

# Text Files

* Small text file, ~1.2MB: [musketeers.txt](./files/musketeers.txt)
* Small text file, ~5.3MB: [shakespeare.txt](./files/shakespeare.txt)
* Collection of large text files [English Texts](http://pizzachili.dcc.uchile.cl/texts/nlang/)

*N.B.* The large files are GZIP compressed. Uncompress them before use with the command

    gunzip english.50MB.gz

# Hints

## Command-line arguments processing

    auto filename = "../files/musketeers.txt"s;
    auto maxWords = 100U;
    auto minSize  = 5U;
    auto outdir   = "./"s;
    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f"s) {
            filename = argv[++k];
        } else if (arg == "-d"s) {
            outdir = argv[++k];
        } else if (arg == "-w"s) {
            maxWords = stoi(argv[++k]);
        } else if (arg == "-s"s) {
            minSize = stoi(argv[++k]);
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << argv[0] << " [-f <file>] [-w <max-words>] [-s <min-size>]" << endl;
            return 1;
        }
    }

## Helpful type definitions

    using WordFreqs = std::unordered_map<std::string, unsigned>;
    using WordFreq  = std::pair<std::string, unsigned>;
    using Pairs     = std::vector<WordFreq>;
    using Strings   = std::vector<std::string>;

By expressing input and output types of the functions in terms of the 
types above, it helps focus on the application problem. 

For example, here is a list of function declarations for the whole
problem, that sub-divides into smaller sub-problems.

    auto load(std::string const& filename, unsigned minSize) -> WordFreqs;
    auto strip(std::string line) -> Strings;
    auto mostFrequent(WordFreqs const& freqs, unsigned maxWords) -> Pairs;
    auto asTags(Pairs pairs) -> Strings;
    auto store(Strings const& tags, std::string const& infile, std::string const& outdir) -> std::string;

Another example, is sorting a sequence of word-frequency pairs

    std::sort(begin(sortable), end(sortable), [](WordFreq const& lhs, WordFreq const& rhs) {...})

## Read line-by-line

    for (std::string line; getline(in, line);) { ... }

where `in` is a istream, such as `cin` or an `ifstream`.

## Read word-by-word

    for (std::string word; in >> word;) { ... }

## Normalized font-size

    auto maxFreq     = pairs.front().second;
    auto minFreq     = pairs.back().second;
    auto maxFontSize = 150.0;
    auto minFontSize = 15.0;
    auto scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);
    ...
    auto normalizedFreq = static_cast<int>(scale * freq );

## How to generate a random HTML color

    auto randColor() -> std::string {
        auto val = std::uniform_int_distribution{0, 255};
        auto HEX = [&val]() {
            auto buf = std::ostringstream{};
            buf << std::setw(2) << std::setfill('0') << std::hex << std::uppercase << val(r);
            return buf.str();
        };
        return "#"s + HEX() + HEX() + HEX();
    }

Colorize all words, was not part of the assignment text. However, it's fun to do anyway.

    <span style="font-size: FREQpx; color: COLOR;">WORD</span>

## HTML prefix

    <html>
      <head>
        <title>Word Cloud</title>
      </head>
      <body>
        <h1>Word Cloud</h1>

## HTML suffix

    </body>
    </html>


# Solution

* [word-cloud.cxx](./solution/src/word-cloud.cxx)
* [CMake file](./solution/CMakeLists.txt)


