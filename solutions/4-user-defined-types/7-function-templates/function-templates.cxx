#include <iostream>

using std::cout;

template<typename Iterator, typename Function>
void foreach(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) fn(*first);
}

template<typename Iterator, typename Function>
void generate(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn();
}

template<typename Iterator, typename Function>
void map(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn(*first);
}

template<typename RetType = long, typename Iterator, typename Function>
RetType reduce(Iterator first, Iterator last, Function fn) {
    RetType result = *first;
    for (++first; first != last; ++first) result = fn(result, *first);
    return result;
}

template<typename RetType, typename Iterator, typename MapperFn, typename ReducerFn>
RetType map_reduce(Iterator first, Iterator last, MapperFn mapper, ReducerFn reducer) {
    RetType result = *first;
    for (++first; first != last; ++first) result = reducer(result, mapper(*first));
    return result;
}

int main() {
    auto const N = 20;
    int        numbers[N]{};

    generate(numbers, numbers + N, [next = 0]() mutable { return ++next, next; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << "\n";

    auto result = map_reduce<long>(numbers, numbers + N, 
                                   [](auto n){return n*n;}, 
                                   [](auto acc, auto n){return acc + n;}
                                   );
    cout << "result: " << result << "\n";
}
