#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>
#include <cctype>

using std::cin;
using std::cout;
using std::string;
using std::istream;

string clean(string txt);
auto load(istream& in) -> std::set<string>;

int main(int argc, char** args) {
    std::set<string> words;
    if (argc == 1) {
        words = load(cin);
    } else {
        auto f = std::ifstream{args[1]};
        words = load(f);
    }

    //for (auto it = words.begin(); it != words.end(); ++it) cout << *it << " ";
    for (auto const& w: words) cout << "'" << w << "'" << " ";
}

auto load(istream& in) -> std::set<string> {
    auto        words = std::set<string>{};
    for (string word; in >> word;) {
        word = clean(word);
        if (!word.empty()) words.insert(word);
    }
    return words;
}

string clean(string txt) {
    auto last = std::remove_if(txt.begin(), txt.end(), [](auto ch) {
        return !::isalpha(ch);
    });
    txt.erase(last, txt.end());
    return txt;
}
