cmake_minimum_required(VERSION 3.18)
project(8_class_templates)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(class-templates class-templates.cxx)
target_compile_options(class-templates PRIVATE ${WARN})


