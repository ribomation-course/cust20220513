#include <iostream>
#include <string>
#include <cstring>
using namespace std::string_literals;
using std::cout;
using std::string;

auto strip(string s) -> string {
    auto      r = ""s;
    for (auto k = 0UL; k < s.size(); ++k) {
        auto ch = s[k];
        if (::isalpha(ch)) r += ch;
    }
    return r;
}

int main() {
    auto txt = "abc ABC 123 .,!"s;
    cout << '"' << txt << "\" ==> \"" << strip(txt) << "\"\n";
}


