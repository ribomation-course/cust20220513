#include <iostream>

using std::cout;

void fn(int a, int& b) {
    cout << "a=" << a << ", b=" << b << "\n";
    b *= 42;
    cout << "a=" << a << ", b=" << b << "\n";
}

int main() {
    auto number = 10;
    cout << "(1) number: " << number << "\n";
    fn(number, number);
    cout << "(2) number: " << number << "\n";
}
