#include <iostream>
#include <string>
#include "math-lib.hxx"

using namespace std::literals;
using std::cout;
using std::endl;
using std::string;
using std::to_string;
namespace rm = ribomation::math;


struct Trace {
    Trace(string const& name_, double x) : name{name_ + "("s + to_string(x) + ")"s} {
        cout << tab() << "[" << name << "] enter\n";
        ++level;
    }

    ~Trace() {
        --level;
        cout << tab() << "[" << name << "] exit\n";
    }

private:
    const string      name;
    inline static int level = 0;
    static string tab() {
        auto      s = ""s;
        for (auto k = level; k > 0; --k) s += "  ";
        return s;
    }
};


void invoke(double x) {
    auto t = Trace{"invoke("s, x};
    try {
        auto result = rm::log_2(x);
        cout << "log[2](" << x << ") = " << result << endl;
    } catch (rm::MathError const& err) {
        auto tx = Trace{"catch("s, x};
        cout << "ERROR: " << err.what() << endl;
    }
}

void compute(double x) {
    auto t = Trace{"compute("s, x};
    invoke(x);
}

void usecase(double x) {
    auto t = Trace{"usecase("s, x};
    compute(x);
}

int main() {
    usecase(1024);
    cout << "----\n";
    usecase(0);
    cout << "----\n";
    usecase(-1);
}
