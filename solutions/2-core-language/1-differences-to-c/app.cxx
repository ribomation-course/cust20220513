#include <iostream>
#include <limits>

using std::cout;
using std::cin;

int main() {
    int count  = 0;
    int sum    = 0;
    int minVal = std::numeric_limits<int>::max();
    int maxVal = std::numeric_limits<int>::min();
    do {
        cout << "Number? ";
        int number;
        cin >> number;
        if (number == 0) break;
        ++count;
        sum += number;
        minVal = std::min(minVal, number);
        maxVal = std::max(maxVal, number);
    } while (true);
    cout << "----\n";
    cout << "# values: " << count << "\n";
    cout << "#min/max: " << minVal << "/" << maxVal << "\n";
    cout << "average : " << (static_cast<double>(sum) / count) << "\n";
}
