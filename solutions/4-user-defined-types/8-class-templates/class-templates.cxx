#include <iostream>
#include <string>
#include <deque>
using std::cout;
using std::string;
using namespace std::literals::string_literals;

template<typename ElemType>
class Queue {
    std::deque<ElemType> storage{};
public:
    void put(ElemType x) { storage.push_back(x); }
    ElemType get() {
        auto x = storage.front();
        storage.pop_front();
        return x;
    }
    auto size() const { return storage.size(); }
    bool empty() const { return storage.empty(); }
};

int main() {
    auto const N = 5;
    {
        auto      q = Queue<int>{};
        for (auto k = 1; k <= N; ++k) q.put(k);
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }
    {
        auto      q = Queue<string>{};
        for (auto k = 1; k <= N; ++k) q.put("str-"s + std::to_string(k));
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }
}
