#include <iostream>
#include "contact.hxx"

namespace rm = ribomation::utils;
using namespace std::string_literals;

int main() {
    auto a = rm::Address{}
            .street("Östermalmstorg 1"s)
            //.post_code("114 42")
            .city("Stockholm");
    
    auto c = rm::Contact{"Jens Riboe"s}
            .email("jens@ribomation.se"s)
            //.twitter("jens_riboe"s)
            .address(a);
    
    std::cout << c.to_string();
}
